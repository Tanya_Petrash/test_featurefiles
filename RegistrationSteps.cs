﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using TechTalk.SpecFlow;

namespace FeatureFiles_SpecFllow
{
    [Binding] //показывает имплементацию шагов
    public class RegistrationSteps
    {
        public IWebDriver driver;

         [Given(@"Registration page is open")]
        public void GivenRegistrationPageIsOpen()
        {
            /*ScenarioContext.Current.Pending();*///   ScenarioContext.Current.Pending не даёт тесты выполнится. Т.е. указывает, что шаг ещё не имплементирован

            //ScenarioContext.Current.Pending() вместо метода этого прописываю свой код
            driver = new ChromeDriver();
            driver.Navigate().GoToUrl("http://3.133.100.46/registration");
            driver.Manage().Window.Maximize(); 
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10); 
        }

        [Given(@"The data is filled in the registration field")]
        public void GivenTheDataIsFilledInTheRegistrationField(Table table)
        {
            //ScenarioContext.Current.Pending();  ScenarioContext передаем информацию между шагами
            var tableLogin = table.Rows[0]["login"]; // работа с таблицей как с матрицей
            var tablePassword = table.Rows[0]["password"];
            
        }
        
        [When(@"I press the button '(.*)' to register")]//Обычно вэн шаги просто делают нужные действия для получения актуального результата.

        //Но считают результат - then шаги.
        public void WhenIPressTheButtonToRegister(string textofnotification)
        {
            IWebElement register_button = driver.FindElement(By.XPath("")); // дописать,когда сайт заработает
        }
        
        [Then(@"The notification '(.*)' is appear on the display")]
        public void ThenTheNotificationIsAppearOnTheDisplay(string textofnotification) //p0 мой ожидаемый результат(текст нотификации)
        {
            string expectedresult = driver.FindElement(By.CssSelector("")).Text;// ДОПИСАТЬ тут прописываю через селениум путь к нотификации

            Assert.AreEqual(expectedresult, "Registration procedure complited successfuly", "it's mistake") ;  // для этого шага обязательно Assert
        }
    }
}
