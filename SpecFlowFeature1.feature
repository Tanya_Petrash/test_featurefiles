﻿@HW_19m @Registrationmodule
Feature: Registration
    In order to log in application of Creative Notes
	As a user of application 
	I want to be able to registrate in application

@p1 @smoke
Scenario Outline: Registration with valid data
	Given Registration page is open
	And The data is filled in the registration field
		| Login   | Password   | Confirm password |
		| <login> | <password> | <password>       |
	When I press the button 'Sign up' to register
	Then The notification 'Registration procedure complited successfuly' is appear on the display
	And User goes to the page of authorization
Examples:
		| login | password    | confirm password |
		| Alya  | qwerty12345 | qwerty12345      |
		| Ivan  | qwert12345  | qwert12345       |
		| Goga  | 12345Goga   | 12345Goga        |

@p1 @NegativeScenario
Scenario Outline: Registration with not valid data
	Given Registration page is open
	And The data is filled in the registration field
		| Login   | Password   | Confirm password |
		| <login> | <password> | <password>       |
	When I press the button 'Sign up' to register
	Then The notification '<notification>' is appear on the display
	And User stays in the same page
Examples:
		| login | password    | confirm password |
		|       | qwerty12345 | qwerty12345      |
		| I     | qwert12345  | qwert12345       |
		| Ivan@ | 12345Goga   | 12345Goga        |
		| Ivan! | 12345Goga   | 12345Goga        |
		| Alya  |             | qwerty12345      |
		| Ivan  | qwerty      | qwerty           |
		| Goga  | !Goga       | !Goga            |
Examples:
		| notification                                                                                                                   |
		| The login should only consist of letters and numbers from 4 to 12 characters long                                              |
		| The password must be from 4 to 12 characters long,consist only of letters and numbers,be sure to contain 1 letter and 1 number |

@p2 @NegativeScenario
Scenario Outline: Registration with valid data what has been already regastrate
	Given Registration page is open
	And Users have been already registrated
	And The data is filled in the registration field
		| Login   | Password   | Confirm password |
		| <login> | <password> | <password>       |
	When I press the button 'Sign up' to register
	Then The notification 'already_reg' is appear on the display
	And User stays in the same page
Examples:
		| login | password    | confirm password |
		| Alya  | qwerty12345 | qwerty12345      |
		| Ivan  | qwert12345  | qwert12345       |
		| Goga  | 12345Goga   | 12345Goga        |

@p2 @Themeapplication
Scenario Outline: Changing theme in application
Given Registration page is open
And Light theme has been chosen
When I press the button 'Dark theme'
Then The theme  of application changed to the dark theme

@p2 @Language
Scenario: Changing interface language in application
Given Registration page is open
And English language of interface has been chosen
When I press the button 'English'
Then The interface language of application changed to the russian language

@p2 @Switchmodules
Scenario: Switch between registration and authorization modules
Given Registration page is open
When I press the button 'Sign in'
Then The authorization page is opened
